
from globals import *


class Game(object):
    walls = list()   #zmienne statyczne
    bullets = list()
    enemies = list()
    player = None
    paused = False

    @staticmethod
    def check_collision(pos):
        x = pos[0]
        y = pos[1]
        all_objects = Game.walls + Game.bullets + Game.enemies
        if Game.player:
            all_objects.append(Game.player)
        for item in all_objects:
            if item.x <= x <= item.x + item.width and item.y <= y < item.y + item.height:
                return True
        return False


def msg(text,pos,size):
    font = pygame.font.SysFont("monospace", size)
    label = font.render(text, 1, BLACK)
    mainWindow.blit(label,pos)





