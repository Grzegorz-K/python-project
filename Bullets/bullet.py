from Blocks import *


class Bullet(object):
    def __init__(self, pos, direct, enemy=True):
        self.x = pos[0]
        self.y = pos[1]
        self.direction = direct
        self.is_enemy = enemy
        pygame.draw.circle(mainWindow, RED, pos, 10)

    def is_hit(self, target):
        if callable(getattr(target, "identify", None)) and self.is_enemy:
            return False
        return target.x <= self.x <= target.x + target.width and target.y <= self.y <= target.y + target.height

    def update(self):
        if self.direction == direction.UP:
            self.y -= bullet_speed
        elif self.direction == direction.DOWN:
            self.y += bullet_speed
        elif self.direction == direction.LEFT:
            self.x -= bullet_speed
        elif self.direction == direction.RIGHT:
            self.x += bullet_speed
        pygame.draw.circle(mainWindow, RED, (self.x, self.y), 2)

        for enemy in Game.enemies:
            if self.is_hit(enemy):
                enemy.get_shot()
                return False
        for wall in Game.walls:
            if self.is_hit(wall):
                wall.get_shot()
                return False
        if self.is_hit(Game.player):
            Game.player.get_shot()
            return False

        return 0 < self.y < display_height and 0 < self.x < display_width
