from enum import Enum
from colors import *
import pygame
import random

display_width = 800
display_height = 640

width_cells = 25
height_cells = 20
cell_size = 32

speed = 3
bullet_speed = 8
enemy_speed = 3


mainWindow = pygame.display.set_mode((display_width, display_height))


class Direction(Enum):
    UP = 0
    LEFT = 1
    DOWN = 2
    RIGHT = 3

direction = Direction.UP