from Blocks import *
from Game import *
from globals import *
import itertools

rand = lambda xx: random.choice(range(xx)) * cell_size  # funkcja lambda
gen = itertools.cycle([1,2,1])


def create_enemy():
    x1 = rand(width_cells)
    y1 = rand(height_cells)

    while Game.check_collision((x1, y1)):
        x1 = rand(width_cells)
        y1 = rand(height_cells)

    return Enemy((x1, y1), next(gen))


def fill_map():

    x1 = rand(width_cells)
    y1 = rand(height_cells)

    add_wall = Game.walls.append  # dekorator

    for x in range(15):
        Game.enemies.append(create_enemy())

    wall_types = [random.choice(range(2)) + 1 for j in range(50)]  # list comprehension
    for x in range(50):
        x1 = rand(width_cells)
        y1 = rand(height_cells)
        add_wall(Wall((x1, y1), wall_types[x]))

    while Game.check_collision((x1, y1)):
        x1 = rand(width_cells)
        y1 = rand(height_cells)
    Game.player = Player((x1, y1))

    print(Game.player)
    print_start()

def clear_map():
    Game.walls[:] = []
    Game.enemies[:] = []
    Game.bullets[:] = []


def game():
    x_change = 0
    y_change = 0

    fill_map()
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -speed
                    y_change = 0
                elif event.key == pygame.K_RIGHT:
                    x_change = speed
                    y_change = 0
                elif event.key == pygame.K_UP:
                    y_change = -speed
                    x_change = 0
                elif event.key == pygame.K_DOWN:
                    y_change = speed
                    x_change = 0
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    y_change = 0
                elif event.key == pygame.K_SPACE:
                    Game.bullets.append(Bullet((Game.player.get_front_pos()), Game.player.direction, False))
                elif event.key == pygame.K_BACKSPACE and Game.paused:
                    clear_map()
                    fill_map()
                    Game.paused=False

        if not Game.paused:
            mainWindow.fill(WHITE)

            to_remove = list(filter(Enemy.update, Game.enemies)) #filter
            for removal in to_remove:
                Game.enemies.remove(removal)
            for wall in Game.walls:
                if wall.update():
                    Game.walls.remove(wall)
            for bullet in Game.bullets:
                x = bullet.update()
                if x == 0:
                    Game.bullets.remove(bullet)

            Game.player.move(x_change, y_change)

            if not Game.enemies:
                Game.paused = True
                msg("You won!", (display_width / 2 - 100, display_height / 2),50)
                msg("Press Backspace to play again", (display_width / 2 - 150, display_height / 2 + 50),30)

            pygame.display.update()
        clock.tick(60)

    pygame.quit()
    quit()


def printer(func):
    def wrapper(*args, **kwargs):
        f = func(*args, **kwargs)
        return f
    return wrapper


@printer #dekorator
def print_start():
    print("Start!")



pygame.init()
pygame.display.set_caption('Python game')
clock = pygame.time.Clock()

game()