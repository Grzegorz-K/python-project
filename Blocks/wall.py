from Blocks import *


class Wall(Block):
    def __init__(self, pos, block_type):
        #super(Block, self).__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.width = 32
        self. height = 32
        self.direction = direction.UP
        self.HP = block_type
        if block_type == 1:
            self.img = pygame.image.load('img/brick.png')
        elif block_type == 2:
            self.img = pygame.image.load('img/steel.jpg')
        else:
            raise "Invalid block type!"


    def get_shot(self):
        self.HP -= 1

    def update(self):
        self.move(0, 0)
        return self.HP <= 0
