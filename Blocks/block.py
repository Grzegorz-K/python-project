from globals import *
from Game import *
from abc import ABC, abstractmethod


class Block(ABC):
   # def __init__(self):
        #self.width = 0
       # self.height = 0
       # self.x = 0
       # self.y = 0
        #self.direction = Direction.UP

    def move(self, dx, dy):
        self.x += dx
        self.y += dy

        collision = False
        for enemy in Game.enemies:
            if self.is_collision(enemy):
                collision = True
                break
        for wall in Game.walls:
            if self.is_collision(wall):
                collision = True
                break

        if Game.player and self.is_collision(Game.player):
            collision = True

        if collision:
            self.x -= dx
            self.y -= dy

        if dx > 0:
            self.direction = direction.RIGHT
        elif dx < 0:
            self.direction = direction.LEFT
        elif dy > 0:
            self.direction = direction.DOWN
        elif dy < 0:
            self.direction = direction.UP

        if self.x + self.width >= display_width:
            self.x = display_width - self.width
        elif self.x < 0:
            self.x = 0
        if self.y < 0:
            self.y = 0
        elif self.y + self.height > display_height:
            self.y = display_height - self.height

        surf = pygame.transform.rotate(self.img, self.direction.value * 90)
        mainWindow.blit(surf, (self.x, self.y))

    def get_front_pos(self):
        size = self.img.get_rect().size
        if self.direction == direction.UP:
            return self.x + int(size[0] / 2), self.y + 1
        if self.direction == direction.DOWN:
            return self.x + int(size[0] / 2), self.y + size[1] + 1
        if self.direction == direction.LEFT:
            return self.x - 1, self.y + int(size[0] / 2)
        if self.direction == direction.RIGHT:
            return self.x + size[0] + 1, self.y + int(size[0] / 2)

    def is_collision(self, other):
        if self == other:
            return False
        return (self.x <= other.x < self.x + self.width or other.x <= self.x < other.x + other.width) and \
               (self.y <= other.y < self.y + self.height or other.y <= self.y < other.y + other.width)

    @abstractmethod
    def get_shot(self):
        pass