from Blocks import *
from Bullets import *


class Enemy(Block):
    def __init__(self, pos, type=1):
        #super(Block, self).__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.direction = Direction.UP


        self.HP = type
        if type == 1:
            self.img = pygame.image.load('img/tank.png')
        elif type == 2:
            self.img = pygame.image.load('img/tank_yellow.png')
        else:
            raise "Invalid Enemy type!"

        self.width = self.img.get_rect().size[0]
        self.height = self.img.get_rect().size[1]


    def update(self):

        if self.HP <= 0:
            return True
        rand = random.choice(range(0, 100))
        if rand == 0 or (rand > 3 and self.direction == direction.RIGHT):
            self.move(enemy_speed, 0)
        elif rand == 1 or (rand > 3 and self.direction == direction.LEFT):
            self.move(-enemy_speed, 0)
        elif rand == 2 or (rand > 3 and self.direction == direction.UP):
            self.move(0, -enemy_speed)
        elif rand == 3 or (rand > 3 and self.direction == direction.DOWN):
            self.move(0, enemy_speed)

        if rand == 50:
            Game.bullets.append(Bullet((self.get_front_pos()), self.direction))

        return False

    def get_shot(self):
        self.HP -= 1

    def identify(self):
        pass