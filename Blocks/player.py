from Blocks.block import *
from globals import *


class Player(Block):
    def __init__(self, pos):
        #super(Block, self).__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.direction = direction.UP
        self.img = pygame.image.load('img/tank_blue.png')
        self.width = self.img.get_rect().size[0]
        self.height = self.img.get_rect().size[1]

    def get_shot(self):
        Game.paused = True
        msg("You are dead :c", (display_width / 2 - 100, display_height / 2),50)
        msg("Press Backspace to play again", (display_width / 2 - 150, display_height / 2 + 50), 30)

    def __str__(self): #magic method
        return "Player pos: " + str(self.x) + ", "+str(self.y)
