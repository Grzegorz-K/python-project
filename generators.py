from itertools import *


def tank_types():
    tanks = cycle([1, 2, 1])
    i = 0
    while True:
        yield tanks(i)
        i += 1

